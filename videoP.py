
# Camera capture open Cv modules
import cv2
import numpy as np


cap= cv2.VideoCapture(0)

fourcc = cv2.VideoWriter_fourcc(*'MJPG')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))


while(cap.isOpened()):
    ret, frame= cap.read()
    if ret==True:

        out.write(frame)

    cv2.imshow('frame',frame)

    if cv2.waitKey(1) & 0XFF==ord('q'):
        break


cap.release()
out.release()
cv2.destroyAllWindows()

